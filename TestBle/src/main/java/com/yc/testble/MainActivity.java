package com.yc.testble;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothAdapter.LeScanCallback;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.List;
import java.util.UUID;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "TestBle";

    private BluetoothManager mBTManager;
    private BluetoothAdapter mBtAdapter;

    private BluetoothDevice mBTDevice;
    private BluetoothGatt mBTGatt;

    private UUID mGattServiceUUID;
    private UUID mCharUUID;
    private TextView mConnetState;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        checkPermission();
        setContentView(R.layout.activity_main);
        mConnetState = findViewById(R.id.tv_connect_state);
        mBTManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        if(mBTManager != null){
            mBtAdapter = mBTManager.getAdapter();
            if (mBtAdapter != null){
                if(!mBtAdapter.isEnabled()){
                    mBtAdapter.enable();
                }
            }
        }
    }

    private void checkPermission(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {
                //请求权限
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,
                                Manifest.permission.ACCESS_FINE_LOCATION},
                        100);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.d(TAG, "onRequestPermissionsResult() called with: requestCode = [" + requestCode + "], permissions = [" + permissions + "], grantResults = [" + grantResults + "]");
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){
            case 100:
                break;
        }
    }

    public void doClick(View view){
        switch (view.getId()){
            case R.id.bt_scan:
                startScan();
                break;
            case R.id.bt_stop_scan:
                stopScan();
                break;
            case R.id.bt_connect:
                connect(mBTDevice);
                break;
            case R.id.bt_disconnect:
                disConnect();
                break;
            case R.id.bt_read:
                readData();
                break;
            case R.id.bt_write:
                writeData();
                break;
        }
    }

    private void startScan() {
        Log.d(TAG, "startScan() called");
        if(mBtAdapter == null){
            return;
        }
        mBtAdapter.startLeScan(mLeScanCallback);
    }

    private void stopScan(){
        Log.d(TAG, "stopScan() called");
        if(mBtAdapter == null){
            return;
        }
        mBtAdapter.stopLeScan(mLeScanCallback);
    }

    private boolean connect(BluetoothDevice btDevice){
        Log.d(TAG, "connect() called with: btDevice = [" + btDevice.getName() + "]");
        if(btDevice == null){
            return false;
        }
        BluetoothGatt gatt = btDevice.connectGatt(this, false, mBtGattCallback);
        if (gatt != null){
            gatt.connect();
            return true;
        }
        else{
            Log.d(TAG, "btGatt = null");
        }
        return false;
    }

    private void disConnect(){
        Log.d(TAG, "disConnect() called");
        if (mBTGatt != null){
            mBTGatt.disconnect();
            mBTGatt.close();
        }
    }

    private void writeData(){
        Log.d(TAG, "writeData() called");
        if(mGattServiceUUID != null && mCharUUID != null && mBTGatt != null){
            BluetoothGattService service = mBTGatt.getService(mGattServiceUUID);
            if(service != null){
                BluetoothGattCharacteristic characteristic = service.getCharacteristic(mCharUUID);
                characteristic.getDescriptors().get(0).setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
                //将指令放置进特征中
                characteristic.setValue(new byte[] {0x55, 0x11});
                //设置回复形式
//                characteristic.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT);
//                characteristic.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_NO_RESPONSE);
                //开始写数据
                mBTGatt.writeCharacteristic(characteristic);
            }
            else{
                Log.d(TAG, "service == null");
            }
        }
        else{
            Log.d(TAG, "uuid or gatt is null");
        }
    }

    private void readData(){
        if(mGattServiceUUID != null && mCharUUID != null && mBTGatt != null){
            BluetoothGattService service = mBTGatt.getService(mGattServiceUUID);
            if(service != null){
                BluetoothGattCharacteristic characteristic = service.getCharacteristic(mCharUUID);
                mBTGatt.readCharacteristic(characteristic);
            }
        }
    }

//    private static final String BT_NAME = "Mambo HR";
    private static final String BT_NAME = "goc-ble";
    private static final String BT_ADD = "C0:0D:18:A1:16:5C";

    LeScanCallback mLeScanCallback = new LeScanCallback() {

        @Override
        public void onLeScan(BluetoothDevice bluetoothDevice, int rssi, byte[] scanRecord) {
            if(!TextUtils.isEmpty(bluetoothDevice.getName())){
                Log.d(TAG, "onLeScan() called with: bluetoothDevice = [" + bluetoothDevice.getName() + "], add = " + bluetoothDevice.getAddress() + "rssi = [" + rssi);
                if(BT_NAME.equals(bluetoothDevice.getName())
                        && BT_ADD.equals(bluetoothDevice.getAddress())){
                    mBTDevice = bluetoothDevice;
                    connect(mBTDevice);
                    stopScan();
                }
            }
        }
    };

    private BluetoothGattCallback mBtGattCallback = new BluetoothGattCallback() {
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            super.onConnectionStateChange(gatt, status, newState);
            Log.d(TAG, "onConnectionStateChange() called with: status = [" + status + "], newState = [" + newState + "]");
            if(newState == BluetoothProfile.STATE_CONNECTED){
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mConnetState.setText("connected");
                    }
                });
                mBTGatt = gatt;
                gatt.discoverServices();//去寻找我们所需要的服务
            }
            else {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mConnetState.setText("disconnected");
                    }
                });
                mBTGatt.disconnect();

                new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        connect(mBTDevice);
                    }
                }, 1000);
            }
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            super.onServicesDiscovered(gatt, status);
            //find services
//            Log.d(TAG, "onServicesDiscovered() called with: gatt = [" + gatt + "], status = [" + status + "]");
            if(status == BluetoothGatt.GATT_SUCCESS){
                //获取连接的
                List<BluetoothGattService> gattServices =gatt.getServices();
                for(BluetoothGattService service: gattServices){
//                    Log.d(TAG, "BluetoothGattService UUID=:" + service.getUuid());
//                    Log.d(TAG, "BluetoothGattService getType=:" + service.getType());
                    List<BluetoothGattCharacteristic> gattCharacteristics = service.getCharacteristics();
                    for (BluetoothGattCharacteristic gattCharacteristic: gattCharacteristics){
//                        Log.d(TAG, "BluetoothGattCharacteristic UUID=:" + gattCharacteristic.getUuid());
                        int property = gattCharacteristic.getProperties();
//                        Log.d(TAG, "BluetoothGattCharacteristic Properties=:" + property);
                        if((property & BluetoothGattCharacteristic.PROPERTY_READ ) > 0
                                && (property & BluetoothGattCharacteristic.PROPERTY_WRITE ) > 0
                                && (property & BluetoothGattCharacteristic.PROPERTY_NOTIFY ) > 0){
                            Log.d(TAG, "Can Read Write UUID=:" + gattCharacteristic.getUuid());
                            mGattServiceUUID = service.getUuid();
                            mCharUUID = gattCharacteristic.getUuid();
                            writeData();
                            return;
                        }
                    }
                }
            }
//            mGattServiceUUID = null;
//            mCharUUID = null;
        }

        @Override
        public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            super.onCharacteristicRead(gatt, characteristic, status);
            Log.d(TAG, "onCharacteristicRead() called with: gatt = [" + gatt + "], characteristic = [" + characteristic + "], status = [" + status + "]");
        }

        @Override
        public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            super.onCharacteristicWrite(gatt, characteristic, status);
            Log.d(TAG, "onCharacteristicWrite() called with: gatt = [" + gatt + "], characteristic = [" + characteristic + "], status = [" + status + "]");
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            super.onCharacteristicChanged(gatt, characteristic);
            Log.d(TAG, "onCharacteristicChanged() called with: gatt = [" + gatt + "], characteristic = [" + characteristic + "]");
        }
    };

}
