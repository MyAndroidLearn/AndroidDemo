package com.yc.testframework;

import android.media.AudioManager;
import android.os.SystemProperties;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

/**
 * 添加Framework方法
 * 1、Module目录下，创建extra目录，将framework.jar拷贝到该目录下
 * 2、Open Module Setting，添加依赖，
 * 3、Module的build.gradle添加以下内容
 * //添加以下代码，使framework_7.0.jar包编译先于android.jar
 gradle.projectsEvaluated {
 tasks.withType(JavaCompile) {
 options.compilerArgs.add('-Xbootclasspath/p:TestFramework\\extra\\framework_7.0.jar')
 }
 }
 *4、修改Module下的xxx.iml,将sdk顺序移到framework后面，或者在build.gradle添加
 * //修改iml的顺序
 preBuild {
 doLast {
 def imlFile = file(project.name + ".iml")
 println 'Change ' + project.name + '.iml order'
 try {
 def parsedXml = (new XmlParser()).parse(imlFile)
 def jdkNode = parsedXml.component[1].orderEntry.find { it.'@type' == 'jdk' }
 parsedXml.component[1].remove(jdkNode)
 def sdkString = "Android API " + android.compileSdkVersion.substring("android-".length()) + " Platform"
 println 'what' + sdkString
 new Node(parsedXml.component[1], 'orderEntry', ['type': 'jdk', 'jdkName': sdkString, 'jdkType': 'Android SDK'])
 groovy.xml.XmlUtil.serialize(parsedXml, new FileOutputStream(imlFile))
 } catch (FileNotFoundException e) {
 // nop, iml not found
 println "no iml found"
 }
 }
 }
 *
 */
public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        String str = AudioManager.VOLUME_CHANGED_ACTION;
        String date = SystemProperties.get("ro.bootimage.build.date");
        Log.d(TAG, "onCreate() called with: str = [" + str + "]" + ", date = " + date);
    }
}
