package com.yc.testrecycleview;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 * Create by YC2 2018.8.21 17:01
 */
public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

    private static final String TAG = "MyAdapter";

    public interface OnItemClickLitener{
        void onItemClick(View view, int position);
        void onItemLongClick(View view , int position);
    }

    private OnItemClickLitener mOnItemClickLitener;
    private List<ItemBean> mDatas;

    public void setOnItemClickLitener(OnItemClickLitener mOnItemClickLitener){
        this.mOnItemClickLitener = mOnItemClickLitener;
    }

    public MyAdapter(List<ItemBean> datas){
        mDatas = datas;
    }

    /**
     * 需要创建ViewHolder时回调
     * @param parent
     * @param viewType
     * @return
     */
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Log.d(TAG, "onCreateViewHolder() called with: viewType = [" + viewType + "]");
        View item = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_recycleview, parent, false);
        return new MyViewHolder(item);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
        Log.d(TAG, "onBindViewHolder() called with: position = [" + position + "]");
        holder.tv.setText(mDatas.get(position).getData());
        if (mDatas.get(position).getHeight() == 0){
            mDatas.get(position).setHeight((int) (100 + Math.random() * 300));
        }
        //随机高度，用以设置瀑布流
        ViewGroup.LayoutParams params = holder.tv.getLayoutParams();
        params.height = mDatas.get(position).getHeight();
        holder.tv.setLayoutParams(params);

        //设置点击事件
        if(mOnItemClickLitener != null){
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mOnItemClickLitener.onItemClick(v, holder.getLayoutPosition());
                }
            });
            holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    mOnItemClickLitener.onItemLongClick(v, holder.getLayoutPosition());
                    return true;
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        Log.d(TAG, "getItemCount() called");
        return mDatas.size();
    }

    public void addItem(int position){
        mDatas.add(position, new ItemBean("Insert " + position));
        //不是用adapter.notifyDataSetChanged()要调用这个函数才有动画
        notifyItemInserted(position);
    }

    public void removeItem(int position){
        mDatas.remove(position);
        notifyItemRemoved(position);
    }

    /**
     * 实现自己的ViewHolder
     */
    public class MyViewHolder extends RecyclerView.ViewHolder{

        private TextView tv;

        public MyViewHolder(View itemView) {
            super(itemView);
            tv = itemView.findViewById(R.id.tv_num);
        }
    }
}
