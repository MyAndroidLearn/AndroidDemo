package com.yc.testrecycleview;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    private RecyclerView mRecyclerView;
    private List<ItemBean> mDatas;
    private MyAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initData();
        mRecyclerView = findViewById(R.id.recycleview);
        LinearLayoutManager layoutManager =  new LinearLayoutManager(this);
            //设置列表方向
            layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        /*//线程布局
        mRecyclerView.setLayoutManager(layoutManager);
        //添加分割线
        mRecyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL_LIST));*/
       /* //网络布局
        mRecyclerView.setLayoutManager(new GridLayoutManager(this, 4));
        mRecyclerView.addItemDecoration(new DividerGridItemDecoration(this));*/

       //瀑布流
        mRecyclerView.setLayoutManager(new StaggeredGridLayoutManager(4, StaggeredGridLayoutManager.VERTICAL));
        mRecyclerView.addItemDecoration(new DividerGridItemDecoration(this));

        //动画
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(mAdapter = new MyAdapter(mDatas));
        mAdapter.setOnItemClickLitener(new MyAdapter.OnItemClickLitener() {
            @Override
            public void onItemClick(View view, int position) {
                Log.d(TAG, "onItemClick() called with:position = [" + position + "]");
                mAdapter.addItem(position);
            }

            @Override
            public void onItemLongClick(View view, int position) {
                Log.d(TAG, "onItemLongClick() called with:position = [" + position + "]");
                mAdapter.removeItem(position);
            }
        });
    }

    public void doClick(View view){
        switch (view.getId()){
            case R.id.bt_add:
                mAdapter.addItem(3);
                break;
        case R.id.bt_remove:
            mAdapter.removeItem(3);
            break;
        }
    }

    protected void initData(){
        mDatas = new ArrayList<ItemBean>();
        for (int i = 'A'; i < 'z'; i++){
            mDatas.add(new ItemBean("" + (char) i));
        }
    }

}
