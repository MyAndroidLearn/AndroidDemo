package com.yc.testrecycleview;

/**
 * Create by YC2 2018.8.20 20:57
 */
public class ItemBean {
    private String data;
    private int height;

    public ItemBean(String data){
        this.data = data;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String getData() {
        return data;
    }
}
