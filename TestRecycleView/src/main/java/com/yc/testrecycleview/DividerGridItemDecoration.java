package com.yc.testrecycleview;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.View;
import android.widget.GridLayout;

/**
 * 网格分隔线
 * Create by YC2 2018.8.16 20:37
 * 不同于线性列表，风格列表和瀑布流列表的Decoration为下方和右方
 */
public class DividerGridItemDecoration extends RecyclerView.ItemDecoration {

    private static final String TAG = "DividerGridItem";
    private static final int[] ATTRS = new int[]{
            //使用系统默认的分割线，可在主题里面自己定义
            android.R.attr.listDivider
    };


    private Drawable mDivider;

    public DividerGridItemDecoration(Context context){
        final TypedArray type = context.obtainStyledAttributes(ATTRS);
        mDivider = type.getDrawable(0);
        type.recycle();
    }


    @Override
    public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
            drawVertical(c, parent, state);
            drawHorizontal(c, parent, state);
    }

    private void drawVertical(Canvas c, RecyclerView parent, RecyclerView.State state){

        final int childCount = parent.getChildCount();
        for (int i = 0; i < childCount; i++)
        {
            final View child = parent.getChildAt(i);

            final RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child
                    .getLayoutParams();
            final int top = child.getTop() - params.topMargin;
            final int bottom = child.getBottom() + params.bottomMargin;
            final int left = child.getRight() + params.rightMargin;
            final int right = left + mDivider.getIntrinsicWidth();

            mDivider.setBounds(left, top, right, bottom);
            mDivider.draw(c);
        }
    }

    private void drawHorizontal(Canvas c, RecyclerView parent, RecyclerView.State state){
        int childCount = parent.getChildCount();
        for (int i = 0; i < childCount; i++){
            final View child = parent.getChildAt(i);
            final RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child
                    .getLayoutParams();
            final int left = child.getLeft() - params.leftMargin;
            final int right = child.getRight() + params.rightMargin
                    + mDivider.getIntrinsicWidth();
            final int top = child.getBottom() + params.bottomMargin;
            final int bottom = top + mDivider.getIntrinsicHeight();
            mDivider.setBounds(left, top, right, bottom);
            mDivider.draw(c);
        }
    }

    /**
     * 可以通过outRect.set()为每个Item设置一定的偏移量，主要用于绘制Decorator
     */
    @Override
    public void getItemOffsets(Rect outRect, int itemPosition, RecyclerView parent) {
        int spanCount = getSpanCount(parent);
        int childCount = parent.getAdapter().getItemCount();
        Log.i(TAG, "childCount = " + childCount);
        //最后一行，则不需要绘制底部
        if(isLastRow(parent, itemPosition, spanCount, childCount)){
            Log.i(TAG, "isLastRow");
            outRect.set(0, 0, mDivider.getIntrinsicWidth(), 0);
        }
        //最后一列，则不需要绘制右边
        else if(isLastColum(parent, itemPosition, spanCount, childCount)){
            Log.i(TAG, "isLastColum");
            outRect.set(0, 0, 0, mDivider.getIntrinsicHeight());
        }
        else{
            outRect.set(0, 0, mDivider.getIntrinsicWidth(), mDivider.getIntrinsicHeight());
        }
    }

    private boolean isLastColum(RecyclerView parent, int itemPosition, int spanCount, int childCount) {
        Log.d(TAG, "isLastColum() called with: itemPosition = [" + itemPosition + "], spanCount = [" + spanCount + "], childCount = [" + childCount + "]");
        RecyclerView.LayoutManager layoutManager = parent.getLayoutManager();
        if(layoutManager instanceof GridLayoutManager){
            //最后一列
            if ((itemPosition + 1) % spanCount == 0){
                return true;
            }
        }
        else if(layoutManager instanceof StaggeredGridLayoutManager){
            int orientation = ((StaggeredGridLayoutManager) layoutManager).getOrientation();
            //纵向滚动
            if(StaggeredGridLayoutManager.VERTICAL == orientation){
                // 如果是最后一列，则不需要绘制右边
                if ((itemPosition + 1) % spanCount == 0){
                    return true;
                }

            }
            //横向滚动
            else{
                childCount = childCount - childCount%spanCount;
                //最后一列
                if (itemPosition >= childCount){
                    return true;
                }
            }
        }
        return false;
    }

    private boolean isLastRow(RecyclerView parent, int itemPosition, int spanCount, int childCount) {
        RecyclerView.LayoutManager layoutManager = parent.getLayoutManager();
        if(layoutManager instanceof GridLayoutManager){
            childCount = childCount - childCount%spanCount;
            //最后一行
            if (itemPosition >= childCount){
                return true;
            }
        }
        else if(layoutManager instanceof StaggeredGridLayoutManager){
            int orientation = ((StaggeredGridLayoutManager) layoutManager).getOrientation();
            //纵向滚动
            if(StaggeredGridLayoutManager.VERTICAL == orientation){
                childCount = childCount - childCount%spanCount;
                //最后一行
                if (itemPosition >= childCount){
                    return true;
                }
            }
            //横向滚动
            else{
                // 如果是最后一行，则不需要绘制底部
                if ((itemPosition + 1) % spanCount == 0){
                    return true;
                }
            }
        }
        return false;
    }

    private int getSpanCount(RecyclerView parent) {
        int spanCount = -1;
        RecyclerView.LayoutManager layoutManager = parent.getLayoutManager();
        if(layoutManager instanceof GridLayoutManager){
            spanCount = ((GridLayoutManager) layoutManager).getSpanCount();
        }
        else if(layoutManager instanceof StaggeredGridLayoutManager){
            spanCount = ((StaggeredGridLayoutManager) layoutManager).getSpanCount();
        }
        Log.d(TAG, "getSpanCount() spanCount = [" + spanCount + "]");
        return spanCount;
    }
    
    
}
