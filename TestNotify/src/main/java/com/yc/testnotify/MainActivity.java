package com.yc.testnotify;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.RemoteViews;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void doClick(View view){
        switch (view.getId()){
            case R.id.bt_show_notify:
                showNotify();
                break;
        }
    }

    private void showNotify() {
        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
        if (bitmap == null){
            Log.d("CustomNotify", "showNotify() bitmap is null");
        }
        NotificationManager notify = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, null)
                //设置小图标(这个必需设置，不然会报错)
//                .setSmallIcon(-1)
                .setSmallIcon(R.mipmap.co_ic_battery_03)
                //显示的图标(如果为空则不显示)
                .setLargeIcon(bitmap)
                //显示的内容（提示信息）
                .setTicker("Usb扫描中...");
        notify.notify(100, builder.build());
    }

}
